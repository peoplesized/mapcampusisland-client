map = L.map('map', {
  center: [0,0],
   zoom: 2
});

var marker;

var layer = L.mapbox.tileLayer('onlyjsmith.i2iaj6g2');
var userId = getOrCreateGUID();
var layerUrl = 'http://onlyjsmith.cartodb.com/api/v2/viz/e952bbe6-cbf7-11e3-8ccd-0e73339ffa50/viz.json';

map.addLayer(layer);
map.locate({watch: true, setView: true, maxZoom: 16});

// cartodb.createVis('map', layerUrl)

map.on('locationfound', onLocationFound);
map.on('locationerror', onLocationError);

function onLocationFound(e) {
  console.log('Found location - POSTing to CartoDB');
  var radius = e.accuracy / 2;
  postData(e.latlng);

  if (marker) {map.removeLayer(marker);}
  marker = L.marker(e.latlng).addTo(map);
}

function onLocationError(e) {
    alert(e.message);
}

function postData(latLng) {
  var 
  lat = latLng.lat,
  lng = latLng.lng;

  var url = "http://mci.ppls.io/api/v2/sql?q=INSERT INTO mapcampusisland (userId, the_geom) VALUES ('"+userId+"', ST_SetSRID(ST_Point("+lng+", "+lat+"),4326))"
  $.get(url, function( data ) {
  });
}

function getOrCreateGUID () {
  id = localStorage.getItem('userId')
  if (id) {
    return id;
  }
  else {
    id = guid();
    localStorage.setItem('userId', id);
    return id
  }
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}